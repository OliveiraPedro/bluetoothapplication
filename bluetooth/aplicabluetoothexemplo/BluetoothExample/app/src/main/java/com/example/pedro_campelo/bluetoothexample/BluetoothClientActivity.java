package com.example.pedro_campelo.bluetoothexample;

import android.app.Activity;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.io.OutputStream;
import java.util.UUID;

/**
 * Created by pedro_campelo on 25/08/2016.
 */
public class BluetoothClientActivity extends Activity{

    private static final String TAG="bluetooth";
    //Precisa utilziar o mesmo UUID que o servidor utilizou para abrir o socket servidor
    private static final UUID uuid=UUID.fromString("e582717f-8c97-405d-aac8-58e3a57ef8f3");
    private BluetoothDevice device;
    private TextView tMsg;
    private OutputStream outputStream;
    private BluetoothSocket socket;

    public void onCreate(Bundle icicle){
        super.onCreate(icicle);
        setContentView(R.layout.enviar_mensagens);

        tMsg=(TextView)findViewById(R.id.tMsg);
        //Device selecionado na lista
        device=getIntent().getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
        TextView nome=(TextView)findViewById(R.id.textViewDeviceName);
        nome.setText(device.getName() + " - " + device.getAddress());

        findViewById(R.id.btnConectar).setOnClickListener(onClickConectar());
        findViewById(R.id.btnEnviar).setOnClickListener(onClickEnviar());
    }

    private View.OnClickListener onClickConectar(){
        return new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                try{
                    //Faz a conexão que utiliza o mesmo UUID que o servidor utilizou
                    socket=device.createRfcommSocketToServiceRecord(uuid);

                    socket.connect();

                    outputStream=socket.getOutputStream();

                    //Se a conexão for feita com sucesso
                    if(outputStream!=null){
                        //Habilita o botão para enviar mensagens
                        findViewById(R.id.btnConectar).setEnabled(false);
                        findViewById(R.id.btnEnviar).setEnabled(true);
                    }
                }catch(IOException e){
                    error(e);
                    Log.e(TAG,"Erro ao conectar:"+e.getMessage(),e);
                }
            }
        };

    }

    private View.OnClickListener onClickEnviar(){
        return new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                String mensagem=tMsg.getText().toString();


                //Passa para o output a mensagem digitada pelo cliente
                try{
                    if(outputStream!=null){
                        outputStream.write(mensagem.getBytes());
                    }
                }catch(IOException e){
                    error(e);
                    Log.e(TAG,"Erro ao conectar:"+e.getMessage(),e);
                }
            }
        };
    }

    private void error(final IOException e){
        Log.e(TAG,"Erro no client:"+e.getMessage(),e);
        runOnUiThread(new Runnable() {

            @Override
            public void run() {
            Toast.makeText(getBaseContext(),"Erro:"+e.getMessage(),Toast.LENGTH_SHORT).show();
                Log.e(TAG, "Erro" + e.getMessage(), e);
            }

        });
    }

    protected void onDestroy(){
        super.onDestroy();

        try{

            if(outputStream!=null){
                outputStream.close();
            }

        }catch(IOException e){

        }

        try{

            if(socket!=null){
                socket.close();
            }

        }catch(IOException e){

        }
    }

}
