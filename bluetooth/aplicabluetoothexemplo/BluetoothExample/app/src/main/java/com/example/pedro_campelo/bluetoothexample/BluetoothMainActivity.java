package com.example.pedro_campelo.bluetoothexample;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

public class BluetoothMainActivity extends Activity implements AdapterView.OnItemClickListener{

    private ListView listView;
    public String[] listaOPBluetooth;
    Intent intencao;

    @Override
    public void onCreate(Bundle bundle){
        super.onCreate(bundle);
        setContentView(R.layout.lista_opcoes);

        List<String> listaOpcoes=new ArrayList<>();

        listaOpcoes.add("Verificar e ativar Bluetooth");
        listaOpcoes.add("Devices pareados");
        listaOpcoes.add("Encontrar device pelo encereço");
        listaOpcoes.add("Buscar devices");
        listaOpcoes.add("Ficar visivel");
        listaOpcoes.add("Iniciar servidor - Aguardar mensagem");
        listaOpcoes.add("Sair");

        ArrayAdapter<String>adapter=new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,listaOpcoes);

        listView=(ListView)findViewById(R.id.listaBluetoothOpcoes);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(this);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {


        switch (position){

            case 0:
                intencao=new Intent(this,BluetoothCheckActivity.class);
                startActivity(intencao);
                break;

            case 1:
                intencao=new Intent(this,BoundedDevices.class);
                startActivity(intencao);
                break;

            case 2:
                intencao=new Intent(this,DeviceSearchAddress.class);
                startActivity(intencao);
                break;

            case 3:
                intencao=new Intent(this,SearchDevices.class);
                startActivity(intencao);
                break;

            case 4:
                intencao=new Intent(this,VisibleDevices.class);
                startActivity(intencao);
                break;

            case 5:
                intencao=new Intent(this,BluetoothServerActivity.class);
                startActivity(intencao);
                break;

            case 6:
                finish();
                break;
        }
    }
}