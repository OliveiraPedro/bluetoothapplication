package com.example.pedro_campelo.bluetoothexample;

import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothSocket;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import java.io.InputStream;
import java.util.UUID;

/**
 * Created by pedro_campelo on 25/08/2016.
 */
public class BluetoothServerActivity extends BluetoothCheckActivity{
    private static final String TAG="bluetooth";
    private BluetoothServerSocket serverSocket;
    private BluetoothSocket socket;
    private boolean running;
    private InputStream in;

    private static final UUID uuid=UUID.fromString("e582717f-8c97-405d-aac8-58e3a57ef8f3");

    public void onCreate(Bundle icicle){
        super.onCreate(icicle);
        setContentView(R.layout.receber_mensagens);
    }
    //Delarado como public pois,como protected estava com erro
    public void onResume(){
        super.onResume();

        //Se o dispositivo tiver bluetooth e se o mesmo estiver ligado
        if(adaptador!=null && adaptador.isEnabled()){
            new ThreadServidor().start();
            running=true;
        }
    }
    //Thread(processo) que controla a conexão entre os dispositivos envolvidor na comunicação via bluetooth
    //Evita que a tela do dispositivo trave
    class ThreadServidor extends Thread{

        public void run(){
            try{
                serverSocket=adaptador.listenUsingRfcommWithServiceRecord("BluetoothExample",uuid);


                //Nesta parte o servidor é bloqueado até que ocorra uma conexão
                try{
                    //Aguardando a conexão de outro aparelho
                    socket=serverSocket.accept();
                }catch(Exception e){
                    //Entra nesta parte do código caso ocorra algum erro
                }

               if(socket!=null){
                   //É feito o encerramento a instancia socketServer, ja que apartir daqui, o servidor já foi criado
                   serverSocket.close();

                   //A instancia socket ja contem informações das classes InputStream e OutputStream para troca de mensagens
                   in=socket.getInputStream();

                   //Recupera informações sobre o dispositivo que solicitou a conexão
                   BluetoothDevice device=socket.getRemoteDevice();
                   updateViewConnect(device);
                   byte[] bytes= new byte[1024];
                   int length;

                   //Cria um laço(loop) para que receba as mensagens do dispositivo que solicitou a conexão ao "servidor"

                   while(running){
                       //Lê as mensagens enviadas(bloqueado até que responda)
                       length=in.read(bytes);
                       String mensagem=new String(bytes,0,length);
                       TextView tMsg=(TextView)findViewById(R.id.tMsg);

                       final String s=tMsg.getText().toString()+mensagem+"\n";
                       updateViewMessage(s);
                   }
               }
            }catch(Exception e){
                //Entra nesta parte do código caso ocorra algum erro
                Log.e(TAG,"Erro no servidor:"+e.getMessage(),e);
                running=false;
            }
        }
    }

    //Exibe a mensagem na tela, informando o nome do dispositivo que soliticou a conexão ao "servidor"
    private void updateViewConnect(final BluetoothDevice device){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                TextView nome=(TextView)findViewById(R.id.textViewDeviceName);
                nome.setText(device.getName()+" - "+device.getAddress());
            }
        });
    }

    //Exibe a mensagem que foi recebida do dispositivo que solicitou a conexão
    private void updateViewMessage(final String s){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                TextView tMsg=(TextView)findViewById(R.id.tMsg);
                tMsg.setText(s);
            }
        });
    }

    public void onDestroy(){
        super.onDestroy();
        running=false;

        try{
            if(socket!=null){
                socket.close();
            }
            if(serverSocket!=null){
                serverSocket.close();
            }
        }catch(Exception e){
            //Entra nesta parte do código caso ocorra algum erro
            Log.e(TAG,"Erro no servidor:"+e.getMessage(),e);
        }
    }

}
