package com.example.pedro_campelo.bluetoothexample;

import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by pedro_campelo on 15/08/2016.
 */
public class BoundedDevices extends BluetoothCheckActivity implements AdapterView.OnItemClickListener{

    protected List<BluetoothDevice> lista;
    private ListView listView;
    ArrayAdapter<String>adaptadorArray;

    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.lista_opcoes);
        listView=(ListView)findViewById(R.id.listaBluetoothOpcoes);
    }

    public void onResume(){
        super.onResume();
        //O parametro do bluetooth é recuperado aqui via herança da classe mãe BluetoothCheckActivity
        if(adaptador!=null){
            //Lista dos aparelhos pareados, ou seja um aparelho já identificado para troca de informações
            lista=new ArrayList<BluetoothDevice>(adaptador.getBondedDevices());
            atualizarLista();
        }
    }

    protected void atualizarLista(){
        //Cria uma lista que recebe o nome de todos os aparelhos pareados
        List<String>aparelhos=new ArrayList<String>();
        for(BluetoothDevice device:lista){
            //Neste exemplo,está variável boolean(verdadeiro ou falso) será sempre true(verdadeiro)
            //,pois está lista é somente dos pareados, ou seja dos aparelhos em que ja houve troca de informações

            boolean pareado=device.getBondState()==BluetoothDevice.BOND_BONDED;
            aparelhos.add(device.getName()+" - "+device.getAddress()+(pareado?" *pareado":""));
        }

        //Cria o adapter para popular o ListView
        adaptadorArray=new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,aparelhos);
        listView.setAdapter(adaptadorArray);
        listView.setOnItemClickListener(this);
    }



    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        //Recupera o device selecionado
        BluetoothDevice device=lista.get(position);
        /*String msg=device.getName()+" - "+device.getAddress();
        Toast.makeText(this,msg,Toast.LENGTH_SHORT).show();*/
        //Direciona o usuário para a tela de envio de mensagens via conexão bluetooth
        Intent intencao=new Intent(this,BluetoothClientActivity.class);
        intencao.putExtra(BluetoothDevice.EXTRA_DEVICE,device);
        startActivity(intencao);
    }
}
