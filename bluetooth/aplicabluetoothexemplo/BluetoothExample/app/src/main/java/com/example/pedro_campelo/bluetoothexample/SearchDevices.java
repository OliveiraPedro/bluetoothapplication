package com.example.pedro_campelo.bluetoothexample;

import android.Manifest;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

/**
 * Created by pedro_campelo on 15/08/2016.
 */

//Esta classe herda o comportamento de como montar e visualizar uma lista, da classe BoundedDevices.
public class SearchDevices extends BoundedDevices{

//Seguindo boas práticas, sempre efetuar a chamada do método cancelDiscovery() antes de iniciar uma busca para garantir que não existam
//várias buscas ao mesmo tempo

//Sempre efeutar busca seguindo o ciclo de vida de uma activity(tela), para garantir que recursos sejam liberados caso seja encerrada.
// Ex:cancelar uma busca dentro do método onDestroy() para que a busca não continue executando após a activity ser encerrada.

//Não ocorreram falhas caso o desenvolvedor chamar duas(por engano) o método cancelDiscovery(), pois não haverão resultados
// para buscar

//É feita a utilização da classe BroadcastReceiver para receber "informações" dos comandos
// ACTION_DISCOVERY_STARTED, ACTTON_FOUND e ACTION_DISCOVERY_FINISHED, que indicam que uma busca iniciou,que um dispositivo foi encontrado
// e que a a busca terminou, respectivamente.

    //Instância da classe ProgressDialog, que gera uma tela de alerta(dialog) para o usuário, junto com um "progresso"
// do que está sendo feito
    private ProgressDialog alerta;
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        //Não é preciso declarar um arquivo de layout para a lista, pois este comportamento é herdado da classe BoundedDevices.
        int permissao;
//Permisso~es de acesso bluetooth após a versao 6.0
        if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.M){
            permissao=this.checkSelfPermission("Manifest.permission.ACCESS_FINE_LOCATION");
            permissao+=this.checkSelfPermission("Manifest.permission.ACCESS_COARSE_LOCATION");

            if(permissao!=0){
                this.requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION,Manifest.permission.ACCESS_COARSE_LOCATION},1001);
            }
        }



        IntentFilter filtro=new IntentFilter();
        filtro.addAction(BluetoothDevice.ACTION_FOUND);
        filtro.addAction(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);
        filtro.addAction(BluetoothAdapter.ACTION_DISCOVERY_STARTED);

        registerReceiver(mReceiver, filtro);

        buscar();
    }


    private void buscar(){
        //Garante que não exista outra busca sendo realizada
        if(adaptador.isDiscovering()){
            adaptador.cancelDiscovery();
        }
        //Inicia a busca
        adaptador.startDiscovery();
        alerta=ProgressDialog.show(this,"Exemplo","Buscando aparelhos bluetooth...",false,true);
    }

    private final BroadcastReceiver mReceiver=new BroadcastReceiver() {

        //Quantidade de dispositivos encontrados
        private int count;

        @Override
        public void onReceive(Context context, Intent intent) {
            String action=intent.getAction();
            //Se um device foi encontrado
            //1
            if(BluetoothDevice.ACTION_FOUND.equals(action)){
                //Recupera o device da intent
                BluetoothDevice device=intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                //Apenas insere na lista os devices que anida não estão pareados
                //2
                if(device.getBondState()!=BluetoothDevice.BOND_BONDED){
                    lista.add(device);
                    Toast.makeText(context, "Encontrou:" + device.getName() + ":" + device.getAddress(), Toast.LENGTH_SHORT).show();
                    count++;
                }//fim 2
            }//fim 1
            // else if 1
            if(BluetoothAdapter.ACTION_DISCOVERY_STARTED.equals(action)){
                //Iniciou a busca
                count=0;
                Toast.makeText(context, "Busca iniciada...", Toast.LENGTH_SHORT).show();
            }//fim else if 1
            //else if 2
            if(BluetoothAdapter.ACTION_DISCOVERY_FINISHED.equals(action)){
                //Terminou a busca
                Toast.makeText(context,"Busca finalizada."+count+" devices encontrados",Toast.LENGTH_SHORT).show();
                alerta.dismiss();
                //Atualiza o componente listView.Agora vai possuir todos os devices pareados, além dos que foram encontrados
                // pela busca.
                atualizarLista();
            }//fim else if 2
        }
    };

    protected void onDestroy(){
        super.onDestroy();
        //Garante que a busca é cancelada ao sair
        if(adaptador!=null){
            adaptador.cancelDiscovery();
        }
        //Cancela o registro do receiver
        this.unregisterReceiver(mReceiver);
    }



}
