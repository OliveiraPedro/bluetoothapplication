package com.example.pedro_campelo.bluetoothexample;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by pedro_campelo on 15/08/2016.
 */
public class BluetoothCheckActivity extends Activity {

    protected BluetoothAdapter adaptador;

    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tela_verifica_bluetooth);

        //Utilização da instancia da classe BluetoothAdapter
        adaptador=BluetoothAdapter.getDefaultAdapter();
       if(adaptador==null){
           Toast.makeText(this,"Bluetooth não disponível neste aparelho!",Toast.LENGTH_SHORT).show();
           //Finalizando a activity se o aparelho não tiver bluetooth
           finish();
        }

    }

    public void onResume(){
        super.onResume();
        //Se o aparelho estiver com o bluetooth ligado
        if(adaptador.isEnabled()){
            TextView texto=(TextView)findViewById(R.id.textViewBluetooth);
            Toast.makeText(this,"Bluetooth está ligado!",Toast.LENGTH_SHORT).show();
        }else{
            //Se o aparelho não estiver com o bluetooth ligado, uma intent será chamada para mostrar um alerta perguntando
            //se o bluetooth deve ser ativado
            Intent intencaoAtivarBluetooth=new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(intencaoAtivarBluetooth, 0);
        }
    }

    public void onActivityResult(int requestCode,int resultCode,Intent data){
        super.onActivityResult(requestCode,resultCode,data);

        //Aqui é possível verificar se o bluetooth foi ligado ou não
        if(resultCode!=Activity.RESULT_OK){
            Toast.makeText(this,"O bluetooth não foi ativado!",Toast.LENGTH_SHORT).show();
        }
    }
}
