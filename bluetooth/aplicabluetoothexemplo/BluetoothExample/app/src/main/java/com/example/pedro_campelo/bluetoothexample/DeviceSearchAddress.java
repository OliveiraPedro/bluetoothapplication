package com.example.pedro_campelo.bluetoothexample;

import android.app.Activity;
import android.bluetooth.BluetoothDevice;
import android.os.Bundle;
import android.widget.Toast;

/**
 * Created by pedro_campelo on 24/08/2016.
 */
public class DeviceSearchAddress extends BluetoothCheckActivity{


    public void onCreate(Bundle icicle){
        super.onCreate(icicle);

        //Se o aparelho tiver o recurso de bluetooth e se o mesmo estiver ativado
        if(adaptador!=null && adaptador.isEnabled()){
            BluetoothDevice device=adaptador.getRemoteDevice("CC:C3:EA:53:2F:3B");

            Toast.makeText(this,device.getName()+" - "+device.getAddress(),Toast.LENGTH_SHORT).show();
        }
    }
}
